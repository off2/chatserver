#!/bin/bash

export JAVA_HOME=/usr/java/default
export PATH=$JAVA_HOME/bin:$PATH

SCRIPT_DIR=`dirname $0`
APP_HOME=$SCRIPT_DIR/..

JAR=$APP_HOME/lib/timecoupon.jar

JVM_FLAGS="-server -Xmx2048m -Xms2048m -XX:+UseConcMarkSweepGC -XX:+UseParNewGC -XX:SurvivorRatio=4 -XX:TargetSurvivorRatio=50 -XX:MaxTenuringThreshold=31 -XX:CompileThreshold=200 -Dfile.encoding=utf8 -verbose:gc -XX:+PrintGCDateStamps -XX:+PrintGCDetails -Xloggc:$APP_HOME/logs/jvm_gc.log"

PORT=80
JMXPORT=11000
JMX_OPTS="-Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=$JMXPORT -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Djava.rmi.server.hostname=`hostname -f`"

TIMECOUPON_OPTS="-Dserver_type=coupon"

echo "Starting TimeCouponAppServer for port $PORT..."
netstat -lnt | awk '{print $4}' | grep ":$PORT" > /dev/null
if [ $? == 0 ]; then
	exit -1
fi

sudo nohup nice java $JVM_FLAGS $JMX_OPTS -Dsun.net.inetaddr.ttl=0 -Dserver.port=$PORT -Dapp.home=$APP_HOME -Dapp.phase=prod $TIMECOUPON_OPTS -jar $JAR >> $APP_HOME/logs/timecoupon_coupon_server.log 2>&1 &

for (( i = 0; i < 8; i= i+1 )) ; do
        NEW_PID=`cat $APP_HOME/logs/pid.$PORT 2> /dev/null`
        if [[ "$NEW_PID" != "" ]]; then
                echo "Started TimeCouponAppServer for port $PORT, pid $NEW_PID ... "
                exit 0;
        fi

        sleep 10
        echo "."
done

#echo "--------TimeCoupon.log--------"
#TAIL_LOG=`tail -50 $APP_HOME/logs/timecoupon_coupon_server.log`
#echo "$TAIL_LOG"
#echo "---------------------------------"
#echo "" 
#echo "Failed to Start TimeCouponAppServer"
#exit -1;