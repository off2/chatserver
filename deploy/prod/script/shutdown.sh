#!/bin/bash

echo "Stopping CouponAppServer..."

SCRIPT_DIR=`dirname $0`
APP_HOME=$SCRIPT_DIR/..

PORT=80
PID=`cat $APP_HOME/logs/pid.$PORT`

if [[ "$PID" == "" ]]; then
	echo; echo "########## SERVER SHUTDOWNED !! (port=$PORT) ##########"; echo
else
	echo "Stopping CouponAppServer for port $PORT, pid $PID ... "

    sudo kill -TERM $PID
	sleep 6
    RUNNING_PID=`ps -ef | grep java | grep $PORT | awk '{print $2}'`
    if [[ "$RUNNING_PID" == "$PID" ]]; then
		echo "kill -9 $PID"; sudo kill -9 $PID
		echo "rm -f $APP_HOME/logs/pid.$PORT";rm -f $APP_HOME/logs/pid.$PORT
    fi
fi