package com.off2.chat.protocol;

import java.lang.reflect.Method;

import com.off2.chat.server.ChatService;
import com.kakao.coupon.struct.*;
import com.off2.chat.struct.JoinRequest;
import com.off2.chat.struct.JoinResponse;
import com.off2.chat.struct.TestRequest;
import com.off2.chat.struct.TestResponse;


public enum ChatProtocol {
	JOIN(JoinRequest.class, JoinResponse.class),
	TEST(TestRequest.class, TestResponse.class);

	
	
	private Class<?> request;
	private Class<?> response;
	private Method method;

	private ChatProtocol(Class<?> req, Class<?> rep) {
		System.out.print("enum made:" + req.getTypeName() + "\n");
		this.request = req;
		this.response = rep;
		for (Method m : ChatService.methods) {
			if (m.getName().equalsIgnoreCase(this.name())) {
				this.method = m;
				break;
			}
		}
		
	}

	public static ChatProtocol getInstance(Object req){
		for(ChatProtocol protocol : ChatProtocol.values()){
			System.out.printf("enum type %s", protocol.name());
			Class<?> klassQ = protocol.getRequest();
			if(klassQ.equals(req.getClass())){
				return protocol;
			}
			
			if(protocol.getResponse()!=null){
				Class<?> klassR = protocol.getResponse();
				if( klassR.equals(req.getClass()))
					return protocol;
			}
		}
		return null;
	}
	
	
	public static ChatProtocol getInstance(String cmd) {
		return valueOf(cmd.toUpperCase());
	}

	public Class<?> getRequest() {
		return request;
	}

	public Class<?> getResponse() {
		return response;
	}


	public Method getMethod() {
		return this.method;

	}

}
