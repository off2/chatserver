package com.off2.chat.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import sun.misc.Signal;
import sun.misc.SignalHandler;

import com.off2.chat.server.BootstrapServer.AppServer;

@SuppressWarnings("restriction")
public class BOSignalHandler implements SignalHandler {
	private Logger _logger = LoggerFactory.getLogger(BOSignalHandler.class);

	public static final String HUP_SIGNAL = "TERM";

	private ApplicationContext _appContext;

	public static BOSignalHandler registerSignal(ApplicationContext context) {
		return new BOSignalHandler(context);
	}

	private BOSignalHandler(ApplicationContext context) {
		Signal killSignal = new Signal(HUP_SIGNAL);
		Signal.handle(killSignal, this);
		this._appContext = context;
	}

	@Override
	public void handle(Signal signal) {
		if (HUP_SIGNAL.equals(signal.getName()))
			kill();
	}

	private void kill() {
		_logger.warn("Signal handler called for signal KILL.");
		AppServer server = (AppServer) _appContext.getBean("appServer");

		if (server != null) {
			if(!server.stop()) {
				_logger.warn("KILL signal failed");
			} else {
				_logger.warn("KILL signal succeeded");

			}

		} else {
			_logger.error("SERVER is NULL.");
		}
	}
	

}
