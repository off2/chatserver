package com.off2.chat.server;


import com.kakao.coupon.struct.*;
import com.off2.chat.struct.JoinRequest;
import com.off2.chat.struct.JoinResponse;
import com.off2.chat.struct.TestRequest;
import com.off2.chat.struct.TestResponse;

import java.lang.reflect.Method;


public interface ChatService {
	
	static public Method[] methods = ChatService.class.getMethods();

	public JoinResponse join(JoinRequest req);
	public TestResponse test(TestRequest req);

	
}
