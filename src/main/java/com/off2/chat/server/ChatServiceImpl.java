package com.off2.chat.server;

import com.off2.chat.struct.JoinRequest;
import com.off2.chat.struct.JoinResponse;
import com.off2.chat.struct.TestRequest;
import com.off2.chat.struct.TestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ChatServiceImpl implements ChatService {
	
	private Logger _logger = LoggerFactory.getLogger(ChatServiceImpl.class);


	@Override
	public JoinResponse join(JoinRequest req ) {
		_logger.info(String.format("join id : %d", req.getId()));
		JoinResponse res = new JoinResponse();
		res.setRet((byte)0);
		return res;
	}

	@Override
	public TestResponse test(TestRequest req) {
		TestResponse res = new TestResponse();
		res.type = req.type;
		res.floatArray = req.floatArray;
		res.text = req.text;
		return res;
	}
}
