package com.off2.chat.server;

import java.net.InetSocketAddress;
import java.util.Date;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import com.off2.chat.util.ChatDateUtils;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFactory;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.channel.group.ChannelGroupFuture;
import org.jboss.netty.channel.group.DefaultChannelGroup;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class ChatNettyServer implements ChatNettyServerMBean,BootstrapServer.AppServer {

	private Logger _logger = LoggerFactory.getLogger(ChatNettyServer.class);

	private int port = 12000;
	private int maxWorkerCount = 32;
	private ChannelFactory channelFactory;
	private CountDownLatch closeLatch;
	private 
	static final ChannelGroup allChannels = new DefaultChannelGroup("chat-server");

	
	@Resource(name="chatServerPipelineFactory")
	ChannelPipelineFactory pipelineFactory;

	public ChatNettyServer() {

	}

	public void setPort(int port) {
		this.port = port;
	}
	
	public void setMaxWorkerCount(int maxWorkerCount) {
		this.maxWorkerCount = maxWorkerCount;
	}

	@Override
	public void start() {
		ServerBootstrap bootstrap = new ServerBootstrap();
		channelFactory = new NioServerSocketChannelFactory(Executors.newCachedThreadPool(), Executors.newCachedThreadPool(), maxWorkerCount);
		bootstrap.setFactory(channelFactory);
		bootstrap.setPipelineFactory(pipelineFactory);
		bootstrap.setOption("reuseAddress", true);
        bootstrap.setOption("child.keepAlive", true);  
	
		Channel channel = bootstrap.bind(new InetSocketAddress(port));
		ChatNettyServer.allChannels.add(channel);
		_logger.info("bind");
	}

	@Override
	public boolean stop() {

		ChannelGroupFuture future = allChannels.close();
		boolean shutDown = future.awaitUninterruptibly(5 , TimeUnit.SECONDS);
		if(!shutDown ){
			return  false;
		}
		channelFactory.releaseExternalResources();
		
		if (closeLatch != null){
			_logger.info("closeLatch down.");
			closeLatch.countDown();
		}
		return true;
	}

	@Override
	public void startAndWaitClose() {
		start();
		closeLatch = new CountDownLatch(1);
		try {
			closeLatch.await();
			_logger.info("closeLatch await finished.");
		} catch (InterruptedException ignore) {
		}

	}

	@Override
	public boolean isStopped() {
		return closeLatch.getCount()==0?true:false;
	}


	@Override
	public String ModDate(String date) {
		if(date == null){
			ChatDateUtils.modTestDate = null;
			return "ok";
		}

		Date d = ChatDateUtils.parseDate(date);
		if(d == null)
			return "fail";

		ChatDateUtils.modTestDate = date;
		return "ok";

	}
}
