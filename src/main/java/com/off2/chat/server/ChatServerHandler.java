package com.off2.chat.server;

import java.lang.reflect.Method;

import javax.annotation.Resource;

import com.off2.chat.struct.TestRequest;
import com.off2.chat.struct.TestResponse;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.off2.chat.broadcast.WriteManager;
import com.off2.chat.protocol.ChatProtocol;

import static com.off2.chat.protocol.ChatProtocol.*;

public class ChatServerHandler extends SimpleChannelHandler {

	static public final int HEAD_SIZE = 14;
	static public final int CMD_SIZE = 10;

	private static Logger _logger = LoggerFactory.getLogger(ChatServerHandler.class);

	@Resource(name = "chatService")
	ChatService chatService;

	static Method[] methods = ChatService.class.getMethods();

	private Gson gson = new Gson();

	@Override
	public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {

		int length = 0;
		int packetId = 0;
		byte[] cmd = new byte[10];
		String cmdStr = null;

		ChannelBuffer cb = (ChannelBuffer) e.getMessage();
		packetId = cb.readInt();
		length = cb.capacity();
		cb.readBytes(cmd);
//		cmdStr = ProtocolUtils.getMethod(cmd);
		cmdStr = new String(cmd).trim();

		ChatProtocol protocol = getInstance(cmdStr);

		Method method = protocol.getMethod();
		Channel channel = ctx.getChannel();

		Object request = null;
		Object response = null;
        request = gson.fromJson(new String((cb.readBytes(length - HEAD_SIZE).array())), protocol.getRequest());
//		CouponSessionInfo info = (CouponSessionInfo) channel.getAttachment();
//        Object ret = method.invoke(couponService, request);
//        if (ret == null ) {
//			_logger.info("response in null");
//            return;
//        }

		switch (protocol) {
			case JOIN:
				break;
			case TEST:
				TestRequest req = (TestRequest)  request;

				if(req.type == 10 ) { // close session
					channel.close();
					return;
				}
				//simple return
				TestResponse res = new TestResponse();
				res.type = req.type;
				res.floatArray = req.floatArray;
				res.text = req.text;
				response = res;
				WriteManager.write(channel, packetId, response);

				if(req.type == 11) { // send back serverpush too
					res.text = "test2";
					WriteManager.write(channel, -1, response);
				}
				break;

		}



	}

	@Override
	public void channelOpen(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
//		Channel channel = ctx.getChannel();
//		CouponSessionInfo info = new CouponSessionInfo();
//		info.setChannel(channel);
//		channel.setAttachment(info);
//		ProtocolLogger.info(channel, "OPEN", 0);
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
		Channel channel = ctx.getChannel();
//		ProtocolLogger.info(channel, "EXCEPTION", 0);
//		System.out.println(e.getCause());
//		_logger.error("exceptionCaught={}", e.getCause());
		channel.close();
	}

	@Override
	public void channelClosed(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
//		Channel channel = ctx.getChannel();
//		CouponSessionInfo info = (CouponSessionInfo) channel.getAttachment();
//		info.processClose();
//		ProtocolLogger.info(channel, "CLOSE", 0);
	}

}
