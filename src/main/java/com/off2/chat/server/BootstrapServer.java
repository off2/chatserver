package com.off2.chat.server;

import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.management.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.management.ManagementFactory;

public class BootstrapServer {

	public static final String APP_HOME_PROP = "app.home";
	public static final String APP_PHASE_PROP = "app.phase";
	public static final String DEFAULT_HOME_PATH = ".";
	public static final String DEFAULT_PHASE = "test";
	public static final String LOG4J_PROPERTY_PATH = "/conf/log4j.properties";
	public static final String PID_PATH = "/logs/pid";
	private static String __homePath = System.getProperty(BootstrapServer.APP_HOME_PROP, DEFAULT_HOME_PATH);
	private static String __phase = System.getProperty(BootstrapServer.APP_PHASE_PROP, DEFAULT_PHASE);

	private static Logger _logger = LoggerFactory.getLogger(BootstrapServer.class);

	public static String getHomePath() {
		return __homePath;
	}

	public static String getPhase() {
		return __phase;
	}
	
	public static void writePid(File pidFile) throws IOException {
		if (pidFile.exists()) {
			System.err.println("pid file aleady exist. cannot start server.");
			System.exit(-1);
		}

		FileWriter fw = null;
		try {
			fw = new FileWriter(pidFile);
			fw.write(getPid());
		} finally {
			if (fw != null)
				fw.close();
		}
	}

	public static String getPid() throws IOException {
		byte[] bo = new byte[100];
		String[] cmd = { "bash", "-c", "echo $PPID" };
		Process p = Runtime.getRuntime().exec(cmd);
		p.getInputStream().read(bo);
		return new String(bo);

	}

	public static void main(String[] args) {

		PropertyConfigurator.configure(BootstrapServer.getHomePath()+  LOG4J_PROPERTY_PATH);
		
		ClassPathXmlApplicationContext appContext = null;
		File pidFile = new File(BootstrapServer.getHomePath() + BootstrapServer.PID_PATH + "." + System.getProperty("server.port"));

		try{

			if (pidFile.exists()) {
				pidFile.delete();
			}
			
			appContext = new ClassPathXmlApplicationContext("applicationContext.xml");
			AppServer appServer = (AppServer) appContext.getBean("appServer");
			BOSignalHandler.registerSignal(appContext);
			BootstrapServer.writePid(pidFile);
//			BootstrapServer.registerMbean(appServer);
			_logger.info("appserver start!");
			appServer.startAndWaitClose();
			appContext.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}finally{
			if (appContext != null) {
					appContext.close();
			}		
			if (pidFile!=null && pidFile.exists()){
				pidFile.delete();
			}
		}
	}

	private static void registerMbean(AppServer app) throws MalformedObjectNameException, NotCompliantMBeanException, InstanceAlreadyExistsException, MBeanRegistrationException {
		MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
		ObjectName name = new ObjectName("com.kakao.coupon.server:type=ChatNettyServer");
		mbs.registerMBean(app, name);
	}
	
	static public interface AppServer {
		public void startAndWaitClose();
		public void start();
		public boolean stop();
		public boolean isStopped();
		
	}

}
