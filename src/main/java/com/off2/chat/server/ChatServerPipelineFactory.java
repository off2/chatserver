package com.off2.chat.server;

import javax.annotation.Resource;

import org.jboss.netty.channel.ChannelHandler;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.handler.codec.frame.LengthFieldBasedFrameDecoder;
import org.jboss.netty.handler.codec.frame.LengthFieldPrepender;
import org.jboss.netty.handler.logging.LoggingHandler;
import org.jboss.netty.logging.InternalLogLevel;
import org.jboss.netty.logging.InternalLoggerFactory;
import org.jboss.netty.logging.Log4JLoggerFactory;


public class ChatServerPipelineFactory implements ChannelPipelineFactory{
	
	static private int MAX_FRAME_SIZE = 1024;
	static private int FRAME_OFFSET = 0;
	static private int FRAME_LENGTH = 4;
	
	@Resource(name="chatServerHandler")
	ChannelHandler chatServerHandler;
	
	
	@Override
	public ChannelPipeline getPipeline() throws Exception {
		ChannelPipeline pipeLine = Channels.pipeline();
		
		//pipeLine.addLast("frameRawDecoder", new RawPacketDebugger());
        InternalLoggerFactory.setDefaultFactory(new Log4JLoggerFactory());
		pipeLine.addLast("logging", new LoggingHandler(InternalLogLevel.INFO));
		pipeLine.addLast("frameDecoder", new LengthFieldBasedFrameDecoder(MAX_FRAME_SIZE, FRAME_OFFSET, FRAME_LENGTH, -FRAME_LENGTH, FRAME_LENGTH));
		pipeLine.addLast("frameEncoder", new LengthFieldPrepender(FRAME_LENGTH, true));
		pipeLine.addLast("couponHandler", chatServerHandler);

		return pipeLine;
	}

}

