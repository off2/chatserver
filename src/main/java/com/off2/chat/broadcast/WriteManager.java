package com.off2.chat.broadcast;

import java.util.concurrent.atomic.AtomicInteger;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.off2.chat.protocol.ChatProtocol;

public class WriteManager {
	static public final int CMD_SIZE = 10;
	static public final int HEAD_SIZE = 14;
	static public final int NEED_GENERATION_PACK_ID = 0;

	static private Logger _logger = LoggerFactory.getLogger(WriteManager.class);

	private static Gson gson = new Gson();
	private static AtomicInteger generatePacketId = new AtomicInteger();

	public static int generatePakcetId() {

		int id = generatePacketId.getAndDecrement();
		while (id >= 0) {
			if (generatePacketId.compareAndSet(id, -1)) {
				id = 1;
				break;
			}
			id = generatePacketId.decrementAndGet();
		}
		return id;

	}

	public static void write(Channel channel, int packetId, Object obj) {

		write0(channel, packetId, obj);


	}

	private static void write0(Channel channel, int packetId, Object obj) {
		ChatProtocol protocol = ChatProtocol.getInstance(obj);
		String cmd = protocol.name();
		String payload = gson.toJson(obj);
		byte[] payloadBytes = payload.getBytes();
		int payloadLength = payloadBytes.length;
		int padding = CMD_SIZE - cmd.length();

		ChannelBuffer buffer = ChannelBuffers.buffer(HEAD_SIZE + payloadLength);
		buffer.writeInt(packetId);
		buffer.writeBytes(cmd.getBytes());
		buffer.writeZero(padding);
		buffer.writeBytes(payloadBytes);
		channel.write(buffer);

	}

}
