package com.off2.chat.struct;

public class DefaultResponse {
	
	protected byte ret;
	protected String ed;
	
	public byte getRet() {
		return ret;
	}

	public void setRet(byte ret) {
		this.ret = ret;
	}

	public String getEd() {
		return ed;
	}

	public void setEd(String ed) {
		this.ed = ed;
	}
	

}
