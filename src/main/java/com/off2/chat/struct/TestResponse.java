package com.off2.chat.struct;

/**
 * Created by jeremy on 29/01/2017.
 */
public class TestResponse extends DefaultResponse {
	public int type;
	public float[] floatArray;
	public String text;
}
