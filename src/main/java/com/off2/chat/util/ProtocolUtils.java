package com.off2.chat.util;

import java.util.Arrays;

public class ProtocolUtils {

	public static String getMethod(byte[] bytes)
	{
	    int i = bytes.length - 1;
	    while (i >= 0 && bytes[i] == 0)
	    {
	        --i;
	    }

	    return new String(Arrays.copyOf(bytes, i + 1));
	}

}
