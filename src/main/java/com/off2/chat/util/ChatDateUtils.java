package com.off2.chat.util;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;

import java.util.Date;


public class ChatDateUtils {
	
	public static String DATE_FORMAT = "yyyy-MM-dd";

	public static String modTestDate;

	public static Date parseDate(String date) {
		try{
			return  DateUtils.parseDate(date, DATE_FORMAT);
		}catch(Exception e){
			///nothing
		}
		return null;
	}
	
	public static String format(Date date){
		return DateFormatUtils.format(date, DATE_FORMAT);
	}
	
	public static String currentFormatDate() {
		return  format(new Date());
	}

	public static String currentFormatDateWithTest() {
		if(modTestDate  != null)
			return modTestDate;

		return  format(new Date());
	}

	public static Date addDays(String date, int days){
		try{
			return  DateUtils.addDays(DateUtils.parseDate(date, DATE_FORMAT), days);
		}catch(Exception e){
			///nothing
		}
		return null;
	}
	
	public static Date addDays(Date date, int days) {
		return  DateUtils.addDays(date, days);
	}
	
	public static String addDaysAndFormat(Date date, int days) {
		return format(addDays(date, days));
	}
	
	public static String addDaysAndFormat(String date, int days) {
		return format(addDays(date, days));
	}

	
}
