import java.nio.ByteBuffer;

import org.junit.Test;




class A {
	private int a;

	public int getA() {
		return a;
	}

	public void setA(int a) {
		this.a = a;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + a;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		A other = (A) obj;
		if (a != other.a)
			return false;
		return true;
	}
	
	
}

class B extends A{
	public int getB() {
		return b;
	}

	public void setB(int b) {
		this.b = b;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + b;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		B other = (B) obj;
		if (b != other.b)
			return false;
		return true;
	}

	private int b;
}

public class TempTest {
	
	@Test
	public void test() {
		byte[] bytes = ByteBuffer.allocate(4).putInt(75).array();
		for(byte b : bytes){
			System.out.print(String.format("%02x",  b));
			
		}
			System.out.println("");
		
		
		
	}
	
	@Test
	public void dectest() {
		
		
		try {
			
			throw new RuntimeException("");
		}catch (RuntimeException e){
			System.out.println("t");
		} catch (Exception e21){
			System.out.println("t2");
			
		}
		
	}

}
